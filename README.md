# oidc-condition-macro

An AWS CloudFormation macro to generate a OIDC provider string that can be replaced in
the trust relationship policy of an [AWS IAM Role for a Kubernetes service account](https://docs.aws.amazon.com/eks/latest/userguide/create-service-account-iam-policy-and-role.html) in an EKS cluster.

The trust relationship must have the form:

``` json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "arn:aws:iam::<AWS_ACCOUNT_ID>:oidc-provider/<OIDC_PROVIDER>"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "<OIDC_PROVIDER>:sub": "system:serviceaccount:<SERVICE_ACCOUNT_NAMESPACE>:<SERVICE_ACCOUNT_NAME>"
        }
      }
    }
  ]
}
```

This macro is used to replace the entire `Condition.StringEquals` value:

``` json
{
  "<OIDC_PROVIDER>:sub": "system:serviceaccount:<SERVICE_ACCOUNT_NAMESPACE>:<SERVICE_ACCOUNT_NAME>"
}
```

This is done by calling the macro in a CloudFormation template 
with the parameters specified below (snippet):

``` yaml
Parameters:
  ServiceAccountNamespace:
    Description: The k8s service account namespace
    Type: String
    Default: default

  ServiceAccountName:
    Description: The k8s service account name
    Type: String
    Default: default

  OIDCProvider:
    Description: The OIDC provider
    Type: String

Resources:
  Role:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Federated: !Sub arn:aws:iam::${AWS::AccountId}:oidc-provider/${OIDCProvider}
            Action: sts:AssumeRoleWithWebIdentity
            Condition:
              StringEquals:
                Fn::Transform:
                  Name: OIDCConditionMacro
                  Parameters:
                    ServiceAccountNamespace: !Ref ServiceAccountNamespace
                    ServiceAccountName: !Ref ServiceAccountName
                    OIDCProvider: !Ref OIDCProvider

```

In this example, it is assumed that the macro is deployed to the specified AWS account and region
with the name `OIDCConditionMacro`, which is the default name for this CloudFormation stack.

A different name can be specified in the deployment command (see below).

The `OIDCProvider` parameter for the EKS cluster can be obtained with the following command:

``` bash
aws eks describe-cluster \
  --profile <aws_profile_name> \
  --region <aws_region> \
  --name <cluster_name> \
  --query "cluster.identity.oidc.issuer" \
  --output text | sed -e "s/^https:\\/\\///"
```

(where `--profile` and `--region` are optional flags).

## Deploy

For deployment, it is necessary to specify a S3 bucket to contain the CloudFormation artifact.

The other arguments are optional:

``` bash
make deploy bucket=<s3_bucket_name> [region=<aws_region>] [profile=<aws_profile_name>] [stack=<custom_macro_name>]`
```

The macro will have the same name as the stack. If no name is specified, the default is `OIDCConditionMacro`.

For example:

`make deploy bucket=myartifactbucketwithauniquename`


## Authors

- **Andre Silva** - [andreswebs](https://gitlab.com/andreswebs)
