.PHONY: clean test build package deploy

TEMPLATE := template.yml
GENERATED := gen/template-gen.yml

ifndef stack
stack := OIDCConditionMacro
endif

ifndef region
region := us-east-1
endif

clean:
	rm -rf ./gen ./bin/* ./vendor Gopkg.lock

test:
	go test ./src

build:
	export GO111MODULE=on
	env GOOS=linux go build -ldflags="-s -w" -o ./bin/oidc-condition ./src/oidc-condition.go

package: clean test build

ifdef bucket
	mkdir -p gen
	aws cloudformation package \
		--s3-bucket ${bucket} \
		--region ${region} \
		--template-file ${TEMPLATE} \
		--output-template-file ${GENERATED}
else
	@echo "specify a deployment bucket"
endif

deploy: package
	aws cloudformation deploy \
		--region ${region} \
		--capabilities CAPABILITY_IAM CAPABILITY_AUTO_EXPAND \
		--template-file ${GENERATED} \
		--stack-name ${stack}

	rm -rf gen
