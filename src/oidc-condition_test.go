package main

import (
	"reflect"
	"testing"
)

func TestGenCondition(t *testing.T) {

	t.Run("all params filled", func(t *testing.T) {

		p := params{
			OIDCProvider:            "testOIDCProvider",
			ServiceAccountName:      "testServiceAccount",
			ServiceAccountNamespace: "testServiceAccountNamespace",
		}

		got, _ := genCondition(p)
		want := condition{
			"testOIDCProvider:sub": "system:serviceaccount:testServiceAccountNamespace:testServiceAccount",
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %v want %v", got, want)
		}

	})

	t.Run("blank params", func(t *testing.T) {

		p := params{}

		got, err := genCondition(p)

		if err == nil {
			t.Errorf("there should be an error")
		}

		if got != nil {
			t.Errorf("%v should be nil", got)
		}

	})

	t.Run("blank OIDCProvider", func(t *testing.T) {

		p := params{
			ServiceAccountName:      "testServiceAccount",
			ServiceAccountNamespace: "testServiceAccountNamespace",
		}

		got, err := genCondition(p)

		if err == nil {
			t.Errorf("there should be an error")
		}

		if got != nil {
			t.Errorf("%v should be nil", got)
		}

	})
	t.Run("blank ServiceAccountName", func(t *testing.T) {

		p := params{
			OIDCProvider:            "testOIDCProvider",
			ServiceAccountNamespace: "testServiceAccountNamespace",
		}

		got, err := genCondition(p)

		if err == nil {
			t.Errorf("there should be an error")
		}

		if got != nil {
			t.Errorf("%v should be nil", got)
		}

	})

	t.Run("blank ServiceAccountNamespace", func(t *testing.T) {

		p := params{
			OIDCProvider:       "testOIDCProvider",
			ServiceAccountName: "testServiceAccount",
		}

		got, err := genCondition(p)

		if err == nil {
			t.Errorf("there should be an error")
		}

		if got != nil {
			t.Errorf("%v should be nil", got)
		}

	})

}
