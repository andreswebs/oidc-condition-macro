package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/lambda"
	log "github.com/sirupsen/logrus"
)

type condition map[string]string

type params struct {
	OIDCProvider            string
	ServiceAccountName      string
	ServiceAccountNamespace string
}

func (p params) validate() error {

	var empty []string

	switch {
	case p.OIDCProvider == "":
		empty = append(empty, "OIDCProvider")
		fallthrough
	case p.ServiceAccountName == "":
		empty = append(empty, "ServiceAccountName")
		fallthrough
	case p.ServiceAccountNamespace == "":
		empty = append(empty, "ServiceAccountNamespace")
	}

	if len(empty) == 0 {
		return nil
	}

	errString := ""

	for _, e := range empty {
		errString += fmt.Sprintf("%s cannot be empty; ", e)
	}

	return errors.New(errString)

}

func genCondition(p params) (condition, error) {

	err := p.validate()
	if err != nil {
		return nil, err
	}

	pvdrKey := fmt.Sprintf("%s:sub", p.OIDCProvider)
	pvdrValue := fmt.Sprintf("system:serviceaccount:%s:%s", p.ServiceAccountNamespace, p.ServiceAccountName)

	c := condition{
		pvdrKey: pvdrValue,
	}

	return c, nil

}

// CfnReq is a request from Cloudformation Macro
type CfnReq struct {
	Region                  string                 `json:"region"`
	AccountID               string                 `json:"accountId"`
	RequestID               string                 `json:"requestId"`
	TransformID             string                 `json:"transformId"`
	Params                  params                 `json:"params"`
	TemplateParameterValues map[string]interface{} `json:"templateParameterValues"`
	Fragment                map[string]interface{} `json:"fragment"`
}

// CfnRes is a response for CloudFormation Macro
type CfnRes struct {
	RequestID string    `json:"requestId"`
	Status    string    `json:"status"`
	Fragment  condition `json:"fragment"`
}

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
}

func handler(req CfnReq) (CfnRes, error) {

	frag, err := genCondition(req.Params)

	var status string

	if err != nil {

		status = "failure"

		log.WithFields(log.Fields{
			"requestId":   req.RequestID,
			"transformId": req.TransformID,
			"status":      status,
		}).Error(err)

		return CfnRes{
			RequestID: req.RequestID,
			Status:    "failure",
		}, err

	}

	status = "success"

	log.WithFields(log.Fields{
		"requestId":   req.RequestID,
		"transformId": req.TransformID,
		"status":      status,
	}).Info()

	res := CfnRes{
		RequestID: req.RequestID,
		Status:    "success",
		Fragment:  frag,
	}

	return res, nil

}

func main() {
	lambda.Start(handler)
}
